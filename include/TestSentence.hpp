/*
 * Copyright (C) 2017 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <string>

namespace pdt
{

	class TestSentence
	{

		struct Mask
		{
			std::string word;

			struct // inclusive
			{
				size_t begin, end;
			} pos;
		};
	public:
		TestSentence(const std::vector<std::string> &hiddenWords);
		
		void addMask(const std::string &word, size_t begin, size_t end);
		void clearMasks();
		std::string reconstruct();
		const std::string &at(size_t pos);
		size_t size() const;

	private:
		void updateVisibleWords();

		bool mustUpdateWords = true;
		std::vector<Mask> masks;
		std::vector<const std::string*> visibleWords;
		const std::vector<std::string> &hiddenWords;
	};
}
