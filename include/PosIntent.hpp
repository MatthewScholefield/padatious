/*
 * Copyright (C) 2017 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <unordered_map>
#include <vector>
#include <string>
#include <sciod/FloatVec.hpp>

namespace pdt
{

	/* 
	 * Matches a particular part of a sentence
	 * Used for identifying capture groups
	 */
	class PosIntent
	{
	public:
		PosIntent(const std::string &name, const std::vector<std::string> &lines);

		const std::string &getName() const;
		float calcProb(const std::vector<std::string> &words, size_t pos, size_t size) const;
		void train(const std::vector<std::string> &lines);

	private:
		sciod::FloatVec createInputs(const std::vector<std::string> &words, size_t pos, size_t size) const;

		static const std::string numWordsKey, numUnknownKey, nullKey;
		std::string name;
		std::unordered_map<std::string, size_t> netIds;
		sciod::NeuralNet net;
	};
}
