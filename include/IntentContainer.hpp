/*
 * Copyright (C) 2016 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <string>
#include "Intent.hpp"

namespace pdt
{

	class IntentContainer
	{
	public:
		IntentContainer();
		void loadIntents(std::vector<std::string> fileNames);
		void loadCaptures(std::vector<std::string> fileNames);
		IntentData calcIntent(const std::string &query);
		IntentData calcIntent(const std::string &query, const std::string &bestIntent);
		std::string bestIntentName(const std::string &query);
		void addIntent(const std::string &name, const std::vector<std::string> &lines);
		void addCapture(const std::string &name, const std::vector<std::string> &lines);
		void addIntents(const std::vector<std::pair<std::string, std::vector<std::string>>> &intents);
		void addCaptures(const std::vector<std::pair<std::string, std::vector<std::string>>> &captures);
		void setNameScope(const std::string &name);
		void train(bool outputProgress = false);
		void clear();

	private:

		bool mustRecalc = false;
		std::string curNameScope = "";
		std::vector<Intent> intents;
		std::vector<std::vector<std::string>> fileLines;
	};
}
