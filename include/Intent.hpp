/*
 * Copyright (C) 2016 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <string>
#include <unordered_map>
#include <sciod/NeuralNet.hpp>

#include "PosIntent.hpp"

namespace pdt
{

	struct Match
	{
		Match(const std::string &key, const std::string &value);
		std::string key, value;
	};

	struct IntentData
	{
		std::string name;
		std::vector<Match> matches;
		float confidence;
		float strength;
	};

	struct MatchData
	{
		float confidence;
		std::vector<Match> matches;
	};

	class Intent
	{
	public:
		Intent(const std::string &name);
		Intent(const std::string &name, const std::vector<std::string> &lines);

		void setNameScope(const std::string &nameScope);
		const std::string &getNameScope() const;
		const std::string &getName() const;

		MatchData tryMatch(std::vector<std::string> words) const;
		void train(const std::vector<std::vector<std::string>> &fileLines, size_t myId);
		static bool isCaptureToken(const std::string &s);
		static std::string getCaptureName(const std::string &captureToken);

	private:
		static const std::string unknownKey, numWordsKey, posMod;

		sciod::FloatVec createInputs(const std::vector<std::string> &words) const;

		std::unordered_map<std::string, size_t> netIds;
		sciod::NeuralNet net;
		std::vector<PosIntent> captureGroups;
		std::string name, nameScope = "";
	};
}
