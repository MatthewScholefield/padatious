# Padatious #

An efficient and agile neural network intent parser written in pure C++.

### Features ###

 - Intents are easy to create
 - Requires a relatively small amount of data
 - Intents run independent of each other
 - Easily detect capture groups (ie. Find the nearest *McDonald's* -> place: McDonals's)
 - Probabilistic
 
 Learn how it works in [the wiki page][wiki-details].

### API Example ###

Here's a simple example of how to use Padatious:

```C++

#include <iostream>
#include <padatious/IntentContainer.hpp>

using namespace std;
using namespace pdt;

int main()
{
	IntentContainer container;
	container.addIntent("hello",{"Hi!", "Hey there.", "Hello!"});
	container.addIntent("goodbye",{"See you later!", "Goodbye.", "I'll talk to you later."});
	container.addIntent("search",{"Search for {query} on {site}.", "Find {query} using {site}."});
	
	container.train();
	
	// Prints "hello"
	cout << container.bestIntentName("Hello there.") << endl;
	
	// Prints "goodbye"
	cout << container.bestIntentName("Talk to you later!") << endl;
	
	// Prints "query: kittens", "site: youtube"
	auto matches = container.calcIntent("Search for kittens using youtube.").matches;
	for (Match &i : matches)
		cout << i.key << ": " << i.value << endl;
}

```

The `train()` method is optional and will automatically be triggered after adding intents. Depending on the amount of intents, this can take a while. Call it like `train(true)` to output a text progress bar during the process.

### Compiling ###

This project has only been tested in Linux although it should compile and work fine under Windows.

First install the build tools, `meson` and `ninja`. On Ubuntu this is `sudo apt-get install python3 ninja-build build-essential && pip3 install --user meson`.

Next, follow the [build instructions for the single dependency, sciod][sciod-build].
Finally, compile and install with:

```
meson build
cd build
ninja
sudo ninja install
```

Now you should be able to link against padatious using pkg-config and include `<padatious/IntentContainer.hpp>` in code.

### Questions or Comments? ###

Feel free to file an issue or contact me at `matthew3311999@gmail.com`.

[wiki-details]:https://github.com/MatthewScholefield/padatious/wiki/Current-Implementation
[sciod-build]:https://github.com/MatthewScholefield/sciod/tree/master#compile

