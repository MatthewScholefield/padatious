/*
 * Copyright (C) 2016 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "IntentContainer.hpp"
#include "tokenizer.hpp"
#include "IntentTool.hpp"

using namespace std;

namespace pdt
{

IntentContainer::IntentContainer() : intents() { }

void IntentContainer::loadIntents(vector<string> fileNames)
{
	for (auto &name : fileNames)
	{
		// TODO: Support Windows
		const auto lines = split(IntentTool::loadFile(name), "\n");
		addIntent(name, lines);
	}
}

void IntentContainer::loadCaptures(std::vector<std::string> fileNames)
{
	for (auto &name : fileNames)
	{
		// TODO: Support Windows
		const auto lines = split(IntentTool::loadFile(name), "\n");
		size_t begin = name.find_last_of("/") + 1; // TODO: Support Windows
		size_t end = name.find(".capture");
		string stripped = name.substr(begin, end - begin);
		addCapture(stripped, lines);
	}
}

void IntentContainer::addIntent(const string &name, const vector<string> &lines)
{
	mustRecalc = true;
	intents.emplace_back(name, lines);
	fileLines.emplace_back(lines);
}

void IntentContainer::addCapture(const std::string &name, const std::vector<std::string> &lines)
{
	; // TODO: Implement
}

void IntentContainer::addIntents(const vector<pair<string, vector<string> > > &intents)
{
	for (const auto &intent : intents)
		addIntent(intent.first, intent.second);
}

void IntentContainer::addCaptures(const std::vector<std::pair<std::string, std::vector<std::string> > > &captures)
{
	for (const auto &capture : captures)
		addCapture(capture.first, capture.second);
}

void IntentContainer::setNameScope(const std::string &name)
{
	curNameScope = name;
}

void IntentContainer::train(bool outputProgress)
{
	mustRecalc = false;
	for (size_t i = 0; i < fileLines.size(); ++i)
	{
		if (outputProgress)
		{
			float propDone = i / (float) fileLines.size();
			const int width = 64 - 3;
			string bar = "[";
			for (int j = 0; j < width * propDone; ++j)
				bar += "=";
			bar += ">";
			for (int j = width * propDone + 1; j < width; ++j)
				bar += " ";
			bar += "]";
			cout << "\r" << bar;
			flush(cout);
		}
		intents[i].train(fileLines, i);
	}
	if (outputProgress)
		cout << endl;
}

void IntentContainer::clear()
{
	mustRecalc = false;
	curNameScope = "";
	intents.clear();
	fileLines.clear();
}

IntentData IntentContainer::calcIntent(const string &query)
{
	if (mustRecalc)
		train();

	auto words = tokenize(query);
	IntentData bestIntent;
	bestIntent.confidence = 0.f;
	float nextBest = 0.f;

	for (Intent &i : intents)
	{
		const MatchData &data = i.tryMatch(words);
		if (bestIntent.confidence < data.confidence)
		{
			nextBest = bestIntent.confidence;
			bestIntent.confidence = data.confidence;
			bestIntent.matches = data.matches;
			bestIntent.name = i.getName();
		}
		else if (data.confidence > nextBest)
			nextBest = data.confidence;
	}
	bestIntent.strength = bestIntent.confidence - nextBest;
	return bestIntent;
}

IntentData IntentContainer::calcIntent(const std::string &query, const string &correctIntent)
{
	if (mustRecalc)
		train();

	auto words = tokenize(query);
	IntentData bestData;
	bestData.confidence = 0.f;
	float matchConf = 0.f;

	for (Intent &i : intents)
	{
		const MatchData &data = i.tryMatch(words);
		if (i.getName().compare(correctIntent) == 0)
			matchConf = data.confidence;
		else if (data.confidence > bestData.confidence)
		{
			bestData.confidence = data.confidence;
			bestData.matches = data.matches;
			bestData.name = i.getName();
		}
	}
	bestData.strength = (matchConf - bestData.confidence) / (matchConf + bestData.confidence);
	return bestData;
}

string IntentContainer::bestIntentName(const string &query)
{
	return calcIntent(query).name;
}

}
