/*
 * Copyright (C) 2017 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <stdexcept>

#include "TestSentence.hpp"

using namespace std;

namespace pdt
{

TestSentence::TestSentence(const std::vector<std::string> &hiddenWords) :
hiddenWords(hiddenWords) { }

void TestSentence::addMask(const std::string &word, size_t begin, size_t end)
{
	masks.push_back({word,
		{begin, end}});
	mustUpdateWords = true;
}

void TestSentence::clearMasks()
{
	masks.clear();
	mustUpdateWords = true;
}

std::string TestSentence::reconstruct()
{
	if (mustUpdateWords)
		updateVisibleWords();
	if (size() == 0)
		return "";
	string str;
	for (size_t i = 0; i < size(); ++i)
		str += at(i) + " ";
	str.pop_back();
	return str;
}

const std::string &TestSentence::at(size_t pos)
{
	if (mustUpdateWords)
		updateVisibleWords();

	return *visibleWords[pos];
}

size_t TestSentence::size() const
{
	return visibleWords.size();
}

void TestSentence::updateVisibleWords()
{
	mustUpdateWords = false;

	enum class Owner
	{
		words,
		mask
	};

	struct SrcData
	{
		Owner owner = Owner::words;
		size_t id;

		const string *get(const vector<string> &words, const vector<Mask> &masks)
		{
			return owner == Owner::words ? &words[id] : &masks[id].word;
		}
		bool operator==(const SrcData &other)
		{
			return owner == other.owner && id == other.id;
		}
		bool operator!=(const SrcData &other)
		{
			return !(*this == other);
		}
	};

	vector<SrcData> wordSrcs(hiddenWords.size());
	for (size_t i = 0; i < wordSrcs.size(); ++i)
		wordSrcs[i].id = i;
	for (size_t maskId = 0; maskId < masks.size(); ++maskId)
	{
		Mask &mask = masks[maskId];
		for (size_t i = mask.pos.begin; i <= mask.pos.end; ++i)
			if (wordSrcs[i].owner == Owner::mask)
				throw logic_error("Overlapping words: " + to_string(i));
			else
				wordSrcs[i] = {Owner::mask, maskId};
	}

	visibleWords.clear();
	SrcData prev = {Owner::words, hiddenWords.size()};
	for (SrcData &data : wordSrcs)
	{
		if (prev != data)
		{
			prev = data;
			visibleWords.emplace_back(data.get(hiddenWords, masks));
		}
	}
}

}
