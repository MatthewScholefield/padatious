/*
 * Copyright (C) 2016 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <limits>
#include <set>
#include <iostream>
#include <bits/stl_set.h>
#include "Intent.hpp"
#include "tokenizer.hpp"
#include "PosIntent.hpp"

using namespace std;
using namespace sciod;

namespace pdt
{

const string Intent::unknownKey = ":unknown:",
		Intent::numWordsKey = ":numWords:",
		Intent::posMod = "-freq";

Match::Match(const std::string& key, const std::string& value) : key(key), value(value) { }

Intent::Intent(const string &name) : name(name) { }

Intent::Intent(const string &name, const vector<string> &lines) : name(name)
{
	std::set<std::string> captureTokens;
	int id = 0;
	for (auto &line : lines)
		for (auto &i : tokenize(line))
		{
			if (isCaptureToken(i))
				captureTokens.emplace(i);
			if (netIds.find(i) == netIds.end())
			{
				netIds.emplace(i, id++);
				netIds.emplace(i + posMod, id++);
			}
		}
	netIds.emplace(unknownKey, id++);
	netIds.emplace(numWordsKey, id++);

	// Create SubIntents
	captureGroups.reserve(captureTokens.size());
	for (const string &i : captureTokens)
		captureGroups.emplace_back(i, lines);

	net.create(netIds.size(), 15, 2, 1);
}

void Intent::setNameScope(const string &nameScope)
{
	this->nameScope = nameScope;
}

const string &Intent::getNameScope() const
{
	return nameScope;
}

const string &Intent::getName() const
{
	return name;
}

FloatVec Intent::createInputs(const vector<string> &words) const
{
	assert(words.size() > 0);
	FloatVec inputs(netIds.size(), 0.f);
	int noMatch = 0;
	for (size_t i = 0; i < words.size(); ++i)
	{
		auto match = netIds.find(words[i]);
		if (match != netIds.end())
		{
			inputs[match->second] = 1.f;
			inputs[netIds.at(words[i] + posMod)] = i / max(1.f, (words.size() - 1.f));
		}
		else
			++noMatch;
	}
	inputs[netIds.at(unknownKey)] = words.size() == 0 ? 0 : (noMatch / (float) words.size());
	inputs[netIds.at(numWordsKey)] = 1.f / words.size();
	return inputs;
}

MatchData Intent::tryMatch(vector<string> words) const
{

	struct Test
	{
		float prob;
		const string *name = nullptr;
		size_t numWords, offset;
	};
	set<string> foundKeys;
	auto findBest = [&foundKeys, this](const vector<string> &words)
	{
		Test best;
		best.numWords = 0;
		best.offset = 0;
		best.prob = 0.5f;
		
		Test test;
		for (const PosIntent &i : captureGroups)
		{
			if (foundKeys.find(i.getName()) != foundKeys.end())
				continue;
			test.name = &i.getName();
			for (test.numWords = 1; test.numWords < words.size(); ++test.numWords)
			{
				for (test.offset = 0; test.offset + test.numWords <= words.size(); ++test.offset)
				{
					test.prob = i.calcProb(words, test.offset, test.numWords);
					if (test.prob > best.prob)
						best = test;
				}
			}
		}
		if (best.name)
			foundKeys.emplace(*best.name);
		return best;
	};

	auto adjust = [&](const Tokens &words, const string &name, size_t offset, size_t numWords)
	{
		Tokens adjustedWords;
		for (size_t i = 0; i < words.size(); ++i)
		{
			if (i == offset)
			{
				adjustedWords.emplace_back(name);
				i += numWords;
				if (i >= words.size())
					break;
			}
			adjustedWords.emplace_back(words[i]);
		}
		return adjustedWords;
	};

	MatchData data;
	while (1)
	{
		Test best = findBest(words);

		if (best.name && best.numWords > 0)
		{
			Tokens tokens(words.begin() + best.offset, words.begin() + best.offset + best.numWords);
			data.matches.emplace_back(getCaptureName(*best.name), reconstruct(tokens));
			words = adjust(words, *best.name, best.offset, best.numWords);
		}
		else
			break;
	}

	data.confidence = net.calcProb(createInputs(words))[0];

	return data;
}

void Intent::train(const vector<vector<string>> &fileLines, size_t myId)
{
	for (auto &i : captureGroups)
		i.train(fileLines[myId]);

	vector<FloatVecIO> vals;

	for (size_t id = 0; id < fileLines.size(); ++id)
		for (auto &line : fileLines[id])
			vals.emplace_back(createInputs(tokenize(line)), FloatVecSingle((int) (id == myId)));

	net.backPropagate(vals, 0.1f, 8.f);
}

bool Intent::isCaptureToken(const string &s)
{
	return s.size() > 3 && s.at(0) == '{' && s.at(s.size() - 1) == '}';
}

std::string Intent::getCaptureName(const string &captureToken)
{
	size_t begin = captureToken.find_first_of('{');
	size_t end = captureToken.find_last_of('}');

	assert(begin != string::npos && end != string::npos);
	return string(captureToken.begin() + begin + 1, captureToken.begin() + end);
}

}
