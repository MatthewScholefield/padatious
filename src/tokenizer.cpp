/*
 * Copyright (C) 2016 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <wctype.h>
#include "tokenizer.hpp"

using namespace std;

namespace pdt
{

/* Adjusts string to a set standard before tokenization
 * 
 * - Changes to lowercase
 * - Formats symbols appropriately (pads with a space)
 * - Does not guarantee only one space between tokens
 */
static void standardizeString(string &str)
{
	for (auto it = str.begin(); it != str.end(); ++it)
		if (iswspace((*it)))
		{
			if (*it != ' ')
				*it = ' ';
		}
		else if (isalnum(*it))
		{
			if (isupper(*it))
				*it = tolower(*it);
		}
		else // Symbol like ? ! or .
			switch (*it)
			{
				case '{':
				case '}': // Used for capture groups (ie. {time})
				case '-': // Keep hyphenated words
					break;
				default:
					it = str.insert(it, ' ');
					it = str.insert(it + 2, ' '); // it + 2 skips symbol
					break;
			}
}

Tokens tokenize(string s)
{
	standardizeString(s);
	Tokens tokens;
	int startWord = -1; // -1 means index is in whitespace between words
	for (size_t i = 0; i < s.length(); ++i)
	{
		if (s.at(i) == ' ')
		{
			if (startWord >= 0)
			{
				tokens.push_back(s.substr(startWord, i - startWord));
				startWord = -1;
			}
		}
		else
		{
			if (startWord < 0)
				startWord = i;
		}
	}
	if (startWord >= 0)
		tokens.push_back(s.substr(startWord, s.length() - startWord));
	return tokens;
}

Tokens split(const string &str, const string &delim)
{
	Tokens tokens;

	size_t prevPos = 0, pos;
	while ((pos = str.find_first_of(delim, prevPos)) != string::npos)
	{
		if (pos > prevPos)
			tokens.push_back(str.substr(prevPos, pos - prevPos));
		prevPos = pos + 1;
	}
	if (prevPos < str.length())
		tokens.push_back(str.substr(prevPos, string::npos));

	return tokens;
}

string reconstruct(const Tokens &tokens, const string &delim)
{
	if (tokens.size() == 0)
		return "";
	string str;
	for (auto &i : tokens)
		str += i + delim;
	str.pop_back();
	return str;
}

}
