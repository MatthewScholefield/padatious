/*
 * Copyright (C) 2017 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <cassert>
#include <sciod/NeuralNet.hpp>

#include "tokenizer.hpp"
#include "PosIntent.hpp"

using namespace std;
using namespace sciod;

namespace pdt
{

const string PosIntent::numWordsKey = ":numWords:",
		PosIntent::numUnknownKey = ":numUnknown:",
		PosIntent::nullKey = ":null:";

PosIntent::PosIntent(const string &name, const vector<string> &lines) : name(name)
{
	int id = 0;
	for (auto &line : lines)
		for (auto &i : tokenize(line))
			if (netIds.find(i) == netIds.end())
				netIds.emplace(i, id++);
	netIds.emplace(numUnknownKey, id++);
	net.create(netIds.size(), 1, 1, 1);
}

const std::string &PosIntent::getName() const
{
	return name;
}

float PosIntent::calcProb(const std::vector<std::string> &words, size_t pos, size_t size) const
{
	return net.calcProb(createInputs(words, pos, size))[0];
}

void PosIntent::train(const std::vector<std::string> &lines)
{
	vector<FloatVecIO> vals;
	for (auto &line : lines)
	{
		Tokens tokens = tokenize(line);
		for (string &i : tokens)
			if (i.compare(name) == 0)
				i = nullKey;
		for (size_t pos = 0; pos < tokens.size(); ++pos)
			for (size_t size = 1; size + pos <= tokens.size(); ++size)
			{
				float out = size == 1 && tokens[pos].compare(nullKey) == 0;
				vals.emplace_back(createInputs(tokens, pos, size),
								FloatVecSingle(out));
			}
	}
	net.backPropagate(vals, 0.1f, 8.f);
}

FloatVec PosIntent::createInputs(const std::vector<std::string> &words, size_t pos, size_t size) const
{
	assert(size > 0);

	FloatVec inputs(netIds.size(), 0.f);
	int numUnknown = 0;
	for (size_t i = 0; i < words.size(); ++i)
	{
		if (i == pos)
			i += size;
		if (i >= words.size())
			break;
		auto result = netIds.find(words[i]);
		if (result == netIds.end())
			++numUnknown;
		else
		{
			float diff = (float) i - pos;
			float centerOffset = i > pos ? (size - 1) : 0;
			inputs[result->second] = 1.f / (diff - centerOffset);
		}
	}
	inputs[netIds.at(numUnknownKey)] = numUnknown / (float) words.size();
	return inputs;
}

}
