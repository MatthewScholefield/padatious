/*
 * Copyright (C) 2017 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "catch.hpp"
#include "TestSentence.hpp"
#include "tokenizer.hpp"

using namespace std;
using namespace pdt;

TEST_CASE("Test Sentence", "[test-sentence]")
{
	auto words = split("this is a test", " ");
	TestSentence ts(words);
	REQUIRE(ts.reconstruct() == "this is a test");

	SECTION("test single")
	{
		ts.addMask("{mask}", 1, 1);
		REQUIRE(ts.reconstruct() == "this {mask} a test");
		ts.addMask("{mask2}", 3, 3);
		REQUIRE(ts.reconstruct() == "this {mask} a {mask2}");
	}
	SECTION("test double")
	{
		ts.addMask("{mask}", 1, 2);
		REQUIRE(ts.reconstruct() == "this {mask} test");
	}
}
