/*
 * Copyright (C) 2017 "Matthew D. Scholefield"
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "catch.hpp"
#include "tokenizer.hpp"

using namespace std;
using namespace pdt;

TEST_CASE("tokenizer", "[tokenizer]")
{

	SECTION("tokenize()")
	{
		auto process = [](string orig) -> string
		{
			return reconstruct(tokenize(orig));
		};
		REQUIRE(process("Hello there!") == "hello there !");
		REQUIRE(process("Here's a test.?") == "here ' s a test . ?");
		REQUIRE(process(" spaces ") == "spaces");
		REQUIRE(process("  multiple  spaces  ") == "multiple spaces");
	}

	SECTION("split()")
	{
		auto verifyDiff = [](string str, string res)
		{
			REQUIRE(reconstruct(split(str, " ")) == res);
		};
		auto verify = [&verifyDiff](string str)
		{
			verifyDiff(str, str);
		};

		verify("Hello there!");
		verify("Here's a test.?");
		verifyDiff(" spaces ", "spaces");
		verifyDiff("  multiple  spaces  ", "multiple spaces");
	}
}
