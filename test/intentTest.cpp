/*
 * Copyright (C) 2016 Matthew D. Scholefield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <map>
#include <string>
#include "catch.hpp"
#include "IntentContainer.hpp"

using namespace std;
using namespace pdt;

TEST_CASE("basic functionality", "[simple]")
{
	IntentContainer container;
	container.addIntent("hello",{"Hi!", "Hey there.", "Hello!"});
	container.addIntent("goodbye",{"See you later!", "Goodbye.", "I'll talk to you later."});

	REQUIRE(container.bestIntentName("Talk to you later!") == "goodbye");
	REQUIRE(container.bestIntentName("Hello there.") == "hello");
}

TEST_CASE("positional weighting", "[positional-weighting]")
{
	IntentContainer container;
	container.addIntent("can-you",{"Can you eat?", "Can you go outside?", "Can you speak?"});
	container.addIntent("you-can",{"You can play.", "You can leave.", "You can have fun."});

	REQUIRE(container.bestIntentName("Can you") == "can-you");
	REQUIRE(container.bestIntentName("You can") == "you-can");
}

// More frequent words should have less significance.

TEST_CASE("word weighting", "[word-weighting]")
{
	IntentContainer container;
	container.addIntent("apple",{"the apple"});
	container.addIntent("orange",{"the orange"});
	container.addIntent("pear",{"the pear"});

	REQUIRE(container.calcIntent("the").confidence < container.calcIntent("apple").confidence);
	REQUIRE(container.calcIntent("the").confidence < container.calcIntent("orange").confidence);
	REQUIRE(container.calcIntent("the").confidence < container.calcIntent("pear").confidence);
}

TEST_CASE("complex behavior", "[complex-tests]")
{
	IntentContainer container;
	container.addIntents({
		{"hello",
			{"Hi!", "Hey there.", "Hello!"}},
		{"hello-ask",
			{"What's up?", "How's it been?", "How are you doing?"}},
		{"goodbye",
			{"See you later!", "Goodbye.", "I'll talk to you later."}},
		{"angry",
			{"Dammit!", "Darn it!", "I hate you!", "You are so stupid!", "I don't like you!", "Shut up!"}},
		{"sad",
			{"I'm sad.", "I'm not feeling that well.", "Oh no!", "Why is life so cruel?", "Today was a bad day."}},
		{"happy",
			{"I'm happy.", "Today was great!", "I'm doing fantastic!", "It was amazing!"}}
	});

	REQUIRE(container.bestIntentName("Hey, what's up?") == "hello-ask");
	REQUIRE(container.bestIntentName("Hi.") == "hello");
	REQUIRE(container.bestIntentName("I hate your guts!") == "angry");
	REQUIRE(container.bestIntentName("It was so fantastic today!") == "happy");
	REQUIRE(container.bestIntentName("I'm feeling pretty bad today.") == "sad");
	REQUIRE(container.bestIntentName("Later!") == "goodbye");
}

TEST_CASE("captures", "[capture-tests]")
{
	IntentContainer container;
	container.addIntent("weather",{"What's it like in {place}?", "What's the weather in {place}?"});
	container.addIntent("music",{"Play some {song}.", "Play the song, {song}."});
	container.addIntent("reminder",{"Remind me to {task}.", "Don't let me forget that {task}."});
	container.addIntent("search",{"Search for {query} on youtube.", "Look up {query} on google."});

	auto checkMatch = [&](string query, string key, string val)
	{
		auto data = container.calcIntent(query);

		SECTION(query)
		{
			REQUIRE(data.matches.size() == 1);
			REQUIRE(data.matches[0].key == key);
			REQUIRE(data.matches[0].value == val);
		}
	};

	checkMatch("What's it like in chicago?", "place", "chicago");
	checkMatch("What's the weather in california?", "place", "california");
	checkMatch("Play some jazz.", "song", "jazz");
	checkMatch("Don't let me forget to go to the store.", "task", "go to the store");
	checkMatch("Look for funny cats on imgur.", "query", "funny cats");
}

TEST_CASE("multiple captures", "[multiple-captures]")
{
	IntentContainer container;
	container.addIntent("calendar",{"Create an event on {date} at {time} to {title}."});
	container.addIntent("search",{"Search for {query} on {site}.", "On {site}, search for {query}.",
								"Find {query} using {site}."});

	auto checkMatches = [&](string query, map<string, string> matches)
	{
		auto data = container.calcIntent(query);

		SECTION(query)
		{
			for (auto &i : data.matches)
			{
				auto result = matches.find(i.key);
				REQUIRE(result != matches.end());
				REQUIRE(i.value == result->second);
			}
		}
	};

	checkMatches("Create an event on june 5th at two oclock to have a picnic.",{
		{"date", "june 5th"},
		{"time", "two oclock"},
		{"title", "have a picnic"}
	});
	checkMatches("Search for cats on youtube.",{
		{"query", "cats"},
		{"site", "youtube"}
	});
	checkMatches("On youtube, search for cats.",{
		{"query", "cats"},
		{"site", "youtube"}
	});
	checkMatches("Using youtube, find for cats.",{
		{"query", "cats"},
		{"site", "youtube"}
	});
}
